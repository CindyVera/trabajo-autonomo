package facci.cindyvera.aplicacion1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.e("onStop","Cindy Katherine Vera Bailon");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.e("onStart","Este mensaje es del metodo onStart")
    }
}
